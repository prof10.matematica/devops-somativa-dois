FROM python:3.8-slim-buster

RUN apt-get update && apt-get install -y wget unzip

RUN wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip 
RUN unzip ngrok-stable-linux-amd64.zip 
RUN mv ngrok /usr/local/bin/ngrok 
RUN rm ngrok-stable-linux-amd64.zip

RUN mkdir /app
WORKDIR /app

COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 5000

CMD ngrok http 5000 && python app.py