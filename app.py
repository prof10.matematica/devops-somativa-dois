from flask import Flask, request
import os

app = Flask(__name__)

@app.route('/')
def index():
    email = request.args.get('email', '')
    senha = request.args.get('senha', '')
    return (
        '<h2>Insira seu email e senha</h2>'
        '<form>'
        '<label>Email:</label>'
        '<input type="text" email="email" value="' + email + '"><br>'
        '<label>Senha:</label>'
        '<input type="password" name="senha" value="' + senha + '"><br>'
        '<input type="button" value="Enviar">'
        '</form>'
    )

if __name__ == '__main__':
    if 'NGROK_URL' in os.environ:
        app.run(host=os.environ['NGROK_URL'], port=5000)
    else:
        app.run(debug=False, port=5000)
